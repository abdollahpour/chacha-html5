#!/bin/sh

# this script with compress all CSS/JS/HTML files and copy the rest require files also

DEST_DIR=dist
HTML_COMPRESSOR_PATH=~/programs/htmlcompressor-1.5.3.jar
YUI_COMPRESS_PATH=~/programs/yuicompressor/yuicompress.sh

# Create destination file if it does not exist

# replace same file with new one
SAME_FILES=(js/libs/* image/*.{png,jpg})
for i in ${SAME_FILES[*]}
do
	FILE=$DEST_DIR/$i
	DIR=${FILE%/*}
	test -d "$DIR" || mkdir -p "$DIR" && cp -ru $i "$FILE"
done

# Compress JS and CSS

FILES=(index.html css/* js/script.js)
for i in ${FILES[*]}
do
	FILE=$DEST_DIR/$i
	DIR=${FILE%/*}
	$YUI_COMPRESS_PATH -o $FILE $i
done

# Compress HTML
FILES=(partials/*)
for i in ${FILES[*]}
do
	java -jar $HTML_COMPRESSOR_PATH --type html -o $DEST_DIR/$i $i
done
